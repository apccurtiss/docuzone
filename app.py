from dataclasses import dataclass
import click
from flask import Flask, redirect, request, render_template, url_for

from flask_wtf import FlaskForm # type: ignore
from wtforms import StringField  # type: ignore
from wtforms.validators import DataRequired, Length # type: ignore
from wtforms.widgets import TextArea # type: ignore

app = Flask(__name__)
app.secret_key = '2389vpa89fgv89asdfv98gap3v'


@dataclass
class Document:
    title: str
    content: str

    def __contains__(self, search: str) -> bool:
        return search.lower() in self.title.lower() or search in self.content.lower()

documents = [
    Document(
        'Groceries',
        'Eggs, milk, bread'
    ),
    Document(
        'TODO',
        'Laundry, clean the kitchen, mow the lawn'
    ),
]

@app.route('/')
def index():
    return render_template('index.html', documents=enumerate(documents))

@app.route('/search')
def search():
    query = request.args.get('query')
    results = [d for d in enumerate(documents) if query in d[1]]
    return render_template('search.html', query=query, documents=results)

@app.route('/document/<id>')
def document(id):
    return render_template('document.html', document=documents[int(id)])

class CreateForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired(), Length(max=32)])
    content = StringField('Content', widget=TextArea(), validators=[DataRequired(), Length(max=2048)])

@app.route('/create', methods=['GET', 'POST'])
def create():
    form = CreateForm()

    if form.validate_on_submit():
        documents.append(Document(
            form.title.data,
            form.content.data))
        return redirect(url_for('index'))  

    return render_template('create.html', form=form)


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--port', default=80)
def main(debug: bool, port: int) -> None:
    app.run(debug=debug, port=port)

if __name__ == '__main__':
    main()